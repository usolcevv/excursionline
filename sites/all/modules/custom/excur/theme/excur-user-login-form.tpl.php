<div class="formenter container" id="user-login-custom">
  <div class="col-sm-12 modal-registration-item">
    <?php print $icon_email; ?>
    <?php print render($form['name']); ?>
  </div>
  <div class="col-xs-12 modal-registration-item">
    <?php print $icon_password; ?>
    <?php print render($form['pass']); ?>
  </div>
  <div class="col-xs-12 modal-registration-item modal-registration-button">
      <?php print render($form['actions']['submit']); ?>
  </div>
</div>

<div class="element-hidden">
  <?php print drupal_render_children($form);?>
</div>
