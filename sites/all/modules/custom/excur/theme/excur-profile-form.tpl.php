<?php if ($user->uid == 1): ?>
  <?php print drupal_render_children($form); ?>
<?php else: ?>
  <div class="col-xs-9 right-line-office">
  <h1 class="office-name"><?php print $account->field_name[LANGUAGE_NONE][0]['value']; ?></h1>
  <h2 class="office-place"><?php print $city . ', ' . $country; ?></h2>
  <h3 class="office-title"><?php print t('My profile'); ?></h3>
  <div class="col-xs-6 profile-part">
    <h3 class="office-place"><?php print t('personal information');?></h3>
    <div id="messege-error-form">  </div>
    <div class="form-group">


      <label class="label-profile"><?php print t('Логин'); ?>*</label>
      <?php print render($form['account']['name']); ?>
      <label class="label-profile"><??><?php print t('E-mail'); ?>*</label>
      <?php print render($form['account']['mail']); ?>
      <label class="label-profile"><?php print t('ФИО'); ?>*</label>
      <?php print render($form['field_name']); ?>
      <div class="mini-label-profile short-mini-lebel"><?php print t('Введите ФИО на родном языке'); ?></div>

      <?php if(isset($guide_role)): ?>
          <label class="label-profile"><?php print t('Название компании');?></label>
          <?php print drupal_render($form['field_company_name']); ?>
      <?php endif; ?>

      <?php if(!isset($guide_role)): ?>
        <div id="div-field-company-name" <?php if($form['company']['#value'] != 2) print 'style="display: none"'?>>
          <label class="label-profile"><?php print t('Название компании');?>*</label>
          <?php print drupal_render($form['field_company_name']); ?>
        </div>
      <?php endif; ?>

      <label class="label-profile"><?php print t('Город'); ?></label>
      <?php print render($form['field_city']); ?>
      <label class="label-profile"><?php print t('Родной язык');?></label>
      <?php print render($form['field_language']); ?>
      <label class="label-profile"><?php print t('Языки')?></label>
      <?php print render($form['field_languages']); ?>
      <div class="mini-label-profile">
        <?php print t('Перечислите через запятую языки, которыми вы владеете например "Русский, Английский"'); ?>
      </div>
      <label class="label-profile">
        <?php print t('Телефон'); ?>
      </label>
      <?php print render($form['field_phone']); ?>
      <label class="label-profile"><?php print t('О себе'); ?></label>
      <?php print render($form['field_description']); ?>
    </div>
    <div class="col-xs-6">
      <?php print render($form['actions']['submit']);?>
    </div>
    <!--    <div class="col-xs-6">
      <?php /*print render($form['actions']['cancel']);*/?>
    </div>-->
  </div>
  <div class="col-xs-6 profile-part">
    <h3 class="office-place">Пароль</h3>
    <div class="form-group">
      <label class="label-profile">Текущий пароль</label>
      <?php print render($form['account']['current_pass']); ?>
      <div class="mini-label-profile">Введите Ваш текущий пароль для смены E-mail адрес или Пароль.
      </div>
      <label class="label-profile">Новый пароль</label>
      <?php print render($form['account']['pass']); ?>
      <div class="mini-label-profile">
        Чтобы изменить текущий пароль, укажите новый пароль в обоих полях.
      </div>
    </div>
    <div class="col-xs-12">
      <h3 class="office-place office-place-config">Настройки</h3>
    </div>

    <?php /*print render($form['guide']); */?>
    <?php /*print render($form['guide_company']); */?>

    <?php if($form['guide']): ?>
      <div class="col-xs-12 offer-config">
        <div class="checkbox">
          <?php print render($form['guide']); ?>
        </div>
      </div>


      <div class="col-xs-12 offer-config" id="radio-guide-company" <?php if($form['guide']['#value'] != 1) print 'style="display: none"'?>>
        <?php print drupal_render($form['company']); ?>
      </div>

    <?php endif; ?>


    <div class="col-xs-12 offer-config">
      <div class="checkbox">
        <?php print render($form['privatemsg']['pm_enable']); ?>
      </div>
    </div>

    <div class="mini-label-profile">
      Отключение личных сообщений не позволит вам отправлять или получать сообщения от других пользователей.
    </div>

    <div class="col-xs-12 offer-config">
      <div class="checkbox">
        <?php print render($form['privatemsg']['pm_send_notifications']); ?>
      </div>
    </div>

  </div>

    <div id="modal-message-guide" class="modal fade avatar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <?php print theme('modal_empty', array('content' => render($form['field_image']) . render($form['actions']['submit'])));?>)); ?>
    </div>

  <div class="element-hidden">
    <?php print drupal_render_children($form); ?>
  </div>

<?php endif; ?>
