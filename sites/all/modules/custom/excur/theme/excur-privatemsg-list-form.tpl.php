<div class="col-xs-9 right-line-office">
  <h1 class="office-name"><?php print $user_name; ?></h1>
  <h2 class="office-place"><?php print $city . ', ' . $country; ?></h2>
  <h3 class="office-title">Мои сообщения</h3>

  <div class="panelc">
    <?php print drupal_render_children($form); ?>
  </div>

</div>