<?php if (!empty($guides)): ?>
  <section class="pop-gid">
    <h3 class="pop-title title-country">
      <?php print t('Popular guides'); ?>
    </h3>
    <div class="pop-gid-content">
      <div class="col-xs-1">
      </div>
      <?php foreach ($guides as $id => $guide): ?>
        <div class="col-xs-2">
          <?php print $guide['image']; ?>
          <div class="pop-gid-name">
            <?php print $guide['title']; ?>
          </div>
          <div class="pop-gid-city">
            <?php print $guide['city']; ?>
          </div>
          <div class="pop-gid-star">
            <?php print $icon_star; ?>
            <?php print $guide['rating']; ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </section>

  <?php if(!empty($trem->field_img_baner__2['und'])): ?>
    <a href="<?php print $trem->field_link_banner_2['und'][0]['value'];?>">
      <div class="container app-parnters prtn">
        <img src="<?php print file_create_url($trem->field_img_baner__2['und'][0]['uri']); ?>">
        <h2><?php print $trem->field_text_banner_2['und'][0]['safe_value'] ?></h2>
        <a class="prnt-al" href="<?php print $trem->field_link_banner_2['und'][0]['value'];?>">Перейти<br> к поиску</a>
      </div>
    </a>
  <?php endif; ?>
<?php endif; ?>
