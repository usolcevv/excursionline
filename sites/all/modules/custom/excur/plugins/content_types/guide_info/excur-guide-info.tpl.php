<section class="gid-header">
</section>
<section class="gid-info">
  <div class="col-xs-4 gid-image">
    <?php print $guide_image; ?>
  </div>
  <div class="col-xs-4 gid-name">
    <div class="gid-name-inner">
      <p class="gid-name-first"><?php print $guide; ?></p>
    </div>
  </div>
  <div class="col-xs-4 gid-contacts">
    <div class="col-xs-12 modal-pay-inner-item">
      <img  src="/<?php print EXCUR_FRONT_THEME_PATH;?>/images/place.png"  class="iconplace excur-contacts-iconplace modal-lebel-icon">
      <div class="modal-place-city" >
        <?php print $city; ?>
      </div>
      <div class="excur-contacts-city modal-lebel">Чехия</div>
    </div>
    <div class="col-xs-12 gid-contacts-item">
      <img  src="/<?php print EXCUR_FRONT_THEME_PATH;?>/images/star.png"  class="iconstar excur-contacts-icon">&nbsp&nbsp&nbsp    <?php print $rating; ?>
    </div>
    <div class="col-xs-12 gid-main-language">
      <p class="language-title"><?php print t('Родной язык:')?></p>
      <p><?php print $language; ?></p>
    </div>
    <div class="col-xs-12 gid-other-language">
      <p class="language-title"><?php print t('Также владею:');?></p>
      <?php foreach($languages as $lang):  ?>
      <p><?php print $lang; ?></p>
      <?php endforeach; ?>
    </div>
    <div class="col-xs-12 gid-other-language">
      <p class="language-title"><?php print t('Номер телефона:');?></p>
        <?php print $number_phone;?>
    </div>
  </div>
  <div class="col-xs-4">
    <?php print $ask_guide; ?>
  </div>
  <div class="col-xs-12 gid-about">
    <?php print $description;?>
  </div>
</section>

<div id="modal-message-guide" class="modal fade message-guide" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <?php print $form; ?>
</div>

