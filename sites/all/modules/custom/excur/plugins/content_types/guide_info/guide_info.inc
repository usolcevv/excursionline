<?php
/**
 * @file
 * Ctools content type new_message.
 */

/**
 * Implements hook_ctools_content_types().
 */
function excur_guide_info_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Guide information'),
    'all contexts' => TRUE,
    'category' => t('Excur'),
    'hook theme' => 'excur_guide_info_content_type_theme',
  );
}

/**
 * Implements hook_content_type_render().
 */
function excur_guide_info_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = theme('excur_guide_info');

  return $block;
}

/**
 * Implements hook_content_type_theme().
 */
function excur_guide_info_content_type_theme(&$theme, $plugin) {
  $theme['excur_guide_info'] = array(
    'path' => $plugin['path'],
    'template' => 'excur-guide-info',
  );
}

/**
 * Process variables for excur-guide-info.tpl.php.
 */
function template_preprocess_excur_guide_info(&$vars) {
  module_load_include('inc', 'privatemsg', 'privatemsg.pages');
  $account = menu_get_object('user');
  $is_company = excur_guide_is_company($account);

  $vars['description'] = $account->field_description['und'][0]['safe_value'];
  if (!empty($account->field_city[LANGUAGE_NONE])) {
    $city = $account->field_city[LANGUAGE_NONE][0]['entity'];
    $vars['city'] = $city->name;
  }

  if (!empty($account->field_certified[LANGUAGE_NONE]) && $account->field_certified[LANGUAGE_NONE][0]['value']) {
    $vars['certified'] = t('Certified guide');
  }

  $vars['guide_image'] = theme('image', array(
    'path' => $account->field_image['und'][0]['uri'],
    'attributes' => array(
      'class' => array('gid-photo')
    )
  ));

  $vars['guide'] = $is_company
    ? $account->field_company_name[LANGUAGE_NONE][0]['safe_value']
    : $account->field_name[LANGUAGE_NONE][0]['safe_value'];

  $vars['language'] = $account->field_languages['und'][0]['taxonomy_term']->name;

  if (!empty($account->field_languages[LANGUAGE_NONE])) {
    foreach ($account->field_languages[LANGUAGE_NONE] as $lang) {
      $vars['languages'][] = $lang['taxonomy_term']->name;
    }
  }

  if ($is_company) {
    if (!empty($account->field_image[LANGUAGE_NONE])) {
      $path = $account->field_image[LANGUAGE_NONE][0]['uri'];
      $theming = 'image_style';
    }
    else {
      $path = EXCUR_FRONT_THEME_PATH . '/images/user-default.png';
      $theming = 'remote_image_style';
    }
    $title_alt = $account->field_name[LANGUAGE_NONE][0]['safe_value'];
    $agent_image = theme($theming, array(
      'style_name' => '238x238',
      'path' => $path,
      'alt' => $title_alt,
      'title' => $title_alt,
    ));

    $vars['agent_image'] = l($agent_image, "user/$account->uid", array('html' => TRUE));
    $vars['agent_name'] = l($account->field_name[LANGUAGE_NONE][0]['value'], "user/$guide->uid");
  }

  // Get current rating.
  $rating = fivestar_get_votes('user', $account->uid);
  if (empty($rating['average'])) {
    $rating = t('Guide has no rating.');
  }
  else {
    $rating = round($rating['average']['value'] / 10, 2);
    $rating = t('!value/10', array('!value' => $rating));
  }
  $vars['rating'] = $rating;

  $vars['number_phone'] = $account->field_phone['und'][0]['value'];

  if (user_is_logged_in()) {
    $vars['ask_guide'] = '<a href="#" class="question-button" id="excur-contacts-guide-button" data-toggle="modal" data-target=".message-guide">' . t('Write to the guide') . '</a>';

    $guide = user_load($account->uid);
      $form = drupal_get_form(
        'privatemsg_new',
        array($guide),
        t('Вопрос гиду'));

    $form['body'] = $form['body']['value'];
    $form['body']['#attributes'] = array('style' =>'height: 145px');
    $form['recipient']['#attributes']['disabled'] = TRUE;
    $form['recipient']['#prefix'] = '<div class="element-hidden">';
    $form['recipient']['#suffix'] = '</div>';
    $form['actions']['submit']['#attributes']['class'][] = 'btn';
    unset($form['token']);
    unset($form['actions']['cancel']);
    $vars['form'] = theme('modal_empty', array('content' => drupal_render($form)));

  }
  else {
    $vars['ask_guide'] = '<a href="#" class="question-button excur-not-login" id="excur-contacts-guide-button">'
      . t('Write to the guide') .
      '</a>';
  }

}
