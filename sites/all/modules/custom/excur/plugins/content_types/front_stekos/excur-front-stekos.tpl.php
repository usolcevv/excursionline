<section id="home1" data-speed="4" data-type="background" class="hidden-xs hidden-sm" style="background-position: 50% -631.25px;">
</section>
<!--<section id="stekos" data-speed="2" data-type="background" class="hidden-sm hidden-xs" style="background-position: 50% -1262.5px;">
  <div class="container stekos-block">
    <div class="row">
      <div class="col-xs-4 stekos-block-left">
        <p>
          <?php /*print t('Stekos'); */?>
        </p>
      </div>
      <div class="col-xs-4 stekos-block-mid">
        <p>
          <?php /*print t('Get your travel'); */?>
        </p>
        <?php /*print $link; */?>

      </div>
      <div class="col-xs-4 stekos-block-right">
        <?php /*print $icon_stekoss; */?>
      </div>
    </div>
  </div>
</section>
-->
<section id="about" data-speed="2" data-type="background" class="hidden-sm hidden-xs" style="background-position: 50% -689px;">
  <h2 class="about-title">
    <?php print t('Benefits'); ?>
  </h2>
  <div class="container">
    <div class="col-xs-6 bord wp5 delay-05 ad-item">
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4 icon-item">
          <?php print $icon1; ?>
        </div>
        <div class="col-xs-10 col-xs-offset-1 bord1">
          <p class="textmid">ПРОСТО И <span style="color:#9e0b0f;">БЕЗОПАСНО</span></p>
        </div>
      </div>
    </div>
    <div class="col-xs-6 bord wp5 delay-05s ad-item">
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4 icon-item">
          <?php print $icon2; ?>
        </div>
        <div class="col-xs-10 col-xs-offset-1 bord1">
          <p class="textmid">ПРЯМОЕ <span style="color:#9e0b0f;">ОБЩЕНИЕ</span> С ГИДОМ</p>
        </div>
      </div>
    </div>
    <div class="col-xs-6 wp5 delay-05s ad-item">
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4 icon-item">
          <?php print $icon3; ?>
        </div>
        <div class="col-xs-10 col-xs-offset-1 bord1">
          <p class="textmid"><span style="color:#9e0b0f;">КРУГЛОСУТОЧНАЯ</span> СЛУЖБА ПОДДЕРЖКИ</p>

        </div>
      </div>
    </div>
    <div class="col-xs-6  bord wp5 delay-05s ad-item">
      <div class="row">
        <div class="col-xs-4 col-xs-offset-4 icon-item">
          <?php print $icon4; ?>
        </div>
        <div class="col-xs-10 col-xs-offset-1 bord1">
          <p class="textmid">ЭКСКУРСИИ, <span style="color:#9e0b0f;">ИНДИВИДУАЛЬНЫЕ</span> ТУРЫ, ТРАНСФЕРЫ, БИЛЕТЫ В МУЗЕЙ</p>
        </div>
      </div>
    </div>
  </div>
</section>