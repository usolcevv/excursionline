<?php
/**
 * @file
 * Ctools content type new_message.
 */

/**
 * Implements hook_ctools_content_types().
 */
function excur_message_view_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Message view'),
    'all contexts' => TRUE,
    'category' => t('Excur'),
    'hook theme' => 'excur_message_view_content_type_theme',
  );
}

/**
 * Implements hook_content_type_render().
 */
function excur_message_view_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = theme('excur_message_view');

  return $block;
}

/**
 * Implements hook_content_type_theme().
 */
function excur_message_view_content_type_theme(&$theme, $plugin) {
  $theme['excur_message_view'] = array(
    'path' => $plugin['path'],
    'template' => 'excur-message-view',
  );
}

/**
 * Process variables for excur-banners.tpl.php.
 */
function template_preprocess_excur_message_view(&$vars) {
  module_load_include('inc', 'privatemsg', 'privatemsg.pages');
  $node = menu_get_object();
  $guide = user_load($node->field_guide[LANGUAGE_NONE][0]['target_id']);

  $vars['th'] = arg(3);


  $messages = privatemsg_thread_load(arg(3));

  $vars['form'] = drupal_get_form('excur_msg_reply_form');

  foreach($messages['messages'] as $mid => $val) {
    $vars['messages'][$mid] = array(
      'author' => $val->author->field_name[LANGUAGE_NONE][0]['safe_value'],
      'time' => date("Y-m-d H:i:s", $val->timestamp),
      'body' => $val->body
    );

    if(!empty($val->author->field_image)) {
      $path = $val->author->field_image[LANGUAGE_NONE][0]['uri'];
    }
    else {
      $path = EXCUR_FRONT_THEME_PATH . '/images/user-default.png';
    }
    $vars['messages'][$mid]['photo'] = theme('image', array(
        'path' => $path,
        'attributes' => array('class' => array('img-chat'))
      )
    );

    foreach($messages['participants'] as $k => $v) {
      if ($messages['user']->uid != $v->uid) {
        $vars['user_from'] =$v->field_name[LANGUAGE_NONE][0]['safe_value'];
      }
    }

  }

}
