<div class="col-xs-9 right-line-office">
  <h1 class="office-name"><b>Виктор</b> Гидовски</h1>
  <h2 class="office-place">Новосибирск, Россия</h2>
  <h3 class="office-title">Мои сообщения</h3>
  <div class="col-xs-12">
    <div class="panel panel-primary panel-message">
      <div class="panel-heading head-panel-message">
        <span class="glyphicon glyphicon-comment"></span>Чат между вами и
        <a href="#"><?php print $user_from; ?></a>
      </div>
      <div class="panel-body">
        <ul class="chat">

          <?php foreach($messages as $val): ?>
          <li class="left clearfix">
            <span class="pull-left">
              <?php print $val['photo']; ?>
            </span>
            <div class="chat-body clearfix">
              <div class="header">
                <strong class="primary-font">
                  <?php print $val['author']; ?>
                </strong>
                <small class="pull-right text-muted">
                  <span class="glyphicon glyphicon-time"></span>
                  <?php print $val['time']; ?>
                </small>
              </div>
              <p>
                <?php print $val['body']; ?>
              </p>
            </div>
          </li>
        <?php endforeach; ?>
        </ul>
      </div>
      <div class="panel-footer">
        <?php print render($form);?>
      </div>
    </div>
  </div>
  <div class="element-hidden">
  </div>