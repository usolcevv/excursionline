
<div class="col-xs-9 right-line-office">
  <h1 class="office-name"><?php print $user_name; ?></h1>
  <?php if(isset($city_country)) : ?>
    <h2 class="office-place"><?php print $city_country; ?></h2>
  <?php endif; ?>
  <h3 class="office-title"><?php print t('Мои закакзы'); ?></h3>
  <div class="col-xs-12 offer-line-select">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#tab-order1" role="tab" data-toggle="tab" aria-controls="confirmed" aria-expanded="true">Подтверждено</a></li>
      <li role="presentation"><a href="#tab-order2" role="tab" data-toggle="tab" aria-controls="no-confirmed" aria-expanded="false">Не подтверждено</a></li>
      <li role="presentation"><a href="#tab-order3" role="tab" data-toggle="tab" aria-controls="rejected" aria-expanded="false">Отклонено</a></li>
      <li role="presentation"><a href="#tab-order4" role="tab" data-toggle="tab" aria-controls="archive" aria-expanded="false">Архив</a></li>
    </ul>
  </div>
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" aria-labelledby="confirmed-tab" id="tab-order1">
      <?php print render($confirmed); ?>
    </div>

    <div role="tabpanel" class="tab-pane fade" aria-labelledby="no-confirmed-tab" id="tab-order2">
      <?php print render($not_confirmed); ?>
    </div>

    <div role="tabpanel" class="tab-pane fade" aria-labelledby="rejected-tab" id="tab-order3">
      <?php print render($rejected); ?>
    </div>

    <div role="tabpanel" class="tab-pane fade"aria-labelledby="archive-tab" id="tab-order4">
      <?php print render($archive); ?>
    </div>

  </div>
</div>

<div id="modal-book" class="modal fade bs-example-pay" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">

</div>
