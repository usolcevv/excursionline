<h3 class="col-xs-12 title-offer title-comment">
  <?php print t('Отзывы'); ?>
</h3>

<div class="col-xs-12 excur-comment">
    <?php if (!empty($comments)): ?>
      <?php print $comments; ?>
    <?php endif ?>
    <?php if (!empty($form)): ?>
      <?php print t('To write a comment, please enter the ticket number.');?>
      <?php print render($form); ?>
    <?php endif ?>
    <?php if(!empty($login)): ?>
      <div class="offer-login">
        <?php print $login; ?>
      </div>
    <?php endif ?>
</div>
