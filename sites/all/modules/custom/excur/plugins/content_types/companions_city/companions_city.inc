<?php


/**
 * Implements hook_ctools_content_types().
 */
function excur_companions_city_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('seeking a companion city'),
    'all contexts' => TRUE,
    'category' => t('Excur'),
    'hook theme' => 'excur_companions_city_content_type_theme',
  );
}

/**
 * Implements hook_content_type_render().
 */
function excur_companions_city_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  //$nid = menu_get_object()->nid;


  $block->content = theme('excur_companions_city');
/*
  $block->content = '<section class="travel-city">
                        <h3 class="pop-title">' . t('Попутчики') . '</h3>
                     <h2 class="pop-title-prefix-travel">' . t('забронированные групповые экскурсии, участники которых ищут попутчиков') . '</h2>';


  $block->content .= '<div class="col-xs-12 travel-companion">' . views_embed_view('content', 'companion_city', arg(2)) . '</div></div>';
*/
  return $block;
}


/**
 * Implements hook_content_type_theme().
 */
function excur_companions_city_content_type_theme(&$theme, $plugin) {
  $theme['excur_companions_city'] = array(
    'path' => $plugin['path'],
    'template' => 'excur-companions-city',
  );
}

/**
 * Process variables for excur-image-slider.tpl.php.
 */
function template_preprocess_excur_companions_city(&$vars) {
  global $language;

  $result = views_get_view_result('content', 'companion_city', arg(2));

  foreach ($result as $k => $node) {
    $node = node_load($node->nid);
    $wrapper = entity_metadata_wrapper('node', $node);
    $wrapper->language($language->language);

    $vars['nodes'][$k]['title'] = $node->title;

    $image_path = $node->field_image[LANGUAGE_NONE][0]['uri'];

    $vars['nodes'][$k]['image'] = l(theme('image', array(
        'path' => $image_path,
        'attributes' => array(
          'class' => array('pop-excur-image')
        )
      )
    ), "node/$node->nid", array('html' => true));

    foreach ($wrapper->field_languages->value() as $lang) {
      $vars['nodes'][$k]['languages'][] = $lang->field_lang_code[LANGUAGE_NONE][0]['value'];
    }


    $price = excur_currency_lowest_price($node);
    $currency = $wrapper->field_currency->value();
    $current_currency = excur_offer_user_currency();
    $price = excur_currency_convert($price, $currency, $current_currency);

    $vars['nodes'][$k]['price'] = $price;
    $vars['nodes'][$k]['currency'] = $current_currency;
    $vars['nodes'][$k]['link'] = "/node/$node->nid";

    // Get current rating.
    $rating = fivestar_get_votes('node', $node->nid);
    if (empty($rating['average'])) {
      $rating = t('Offer is unrated.');
    } else {
      $rating = round($rating['average']['value'] / 100, 2);
      $rating = preg_replace('/\./', ',', $rating);
    }
    $vars['nodes'][$k]['rating'] = $rating;
  }
}