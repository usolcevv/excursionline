<?php if($nodes): ?>
  <section class="travel-city">
    <h3 class="pop-title">
      <?php print t('Попутчики') ?>
    </h3>
    <h2 class="pop-title-prefix-travel">
      <?php print t('забронированные групповые экскурсии, участники которых ищут попутчиков'); ?>
    </h2>

    <?php foreach($nodes as $node):?>
      <div class="col-xs-3 city-travel-item">
        <div class="city-travel-item-inner">
          <?php print $node['image']; ?>
          <div class="pop-excur-row">
            <div class="col-xs-3 pop-excur-star">
              <img src="<?php print file_create_url(EXCUR_FRONT_THEME_PATH . '/images/star.png');?>" class="iconstar">&nbsp;&nbsp;&nbsp;<?php print $rating; ?>
            </div>
            <div class="col-xs-9 pop-excur-language">
              <?php foreach($node['languages'] as $lang): ?>
                <img class="flag flag-<?php print $lang; ?>"/>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="pop-excur-name">
            <?php print $node['title'] ?>
          </div>
          <div class="pop-excur-price">
            <?php print $node['price']; ?>
            <span class="currency">
        <?php print $node['currency']; ?>
      </span>
          </div>
          <a href="<?php print $node['link'];?>">
            <div class="travel-city-about" >
              <img src="<?php print file_create_url(EXCUR_FRONT_THEME_PATH . '/images/question.png');?>" class="iconq">&nbsp&nbsp<?php print t('Подробней'); ?>
            </div>
          </a>
        </div>
      </div>
    <?php endforeach; ?>
  </section>
<?php endif; ?>