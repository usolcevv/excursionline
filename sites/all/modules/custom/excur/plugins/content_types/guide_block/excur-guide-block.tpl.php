<div class="col-xs-3 excur-contacts-item-gid">
  <?php if (!empty($guide_image)): ?>
    <?php print $guide_image; ?>
  <?php endif; ?>
  <?php if(!empty($ask_guide)): ?>
    <?php print $ask_guide; ?>
  <?php endif; ?>
</div>

<div id="modal-message-guide" class="modal fade message-guide" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <?php print $form; ?>
</div>