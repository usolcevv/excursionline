<?php
/**
 * @file
 * Ctools content type new_message.
 */

/**
 * Implements hook_ctools_content_types().
 */
function excur_guide_block_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Guide block'),
    'all contexts' => TRUE,
    'category' => t('Excur'),
    'hook theme' => 'excur_guide_block_content_type_theme',
  );
}

/**
 * Implements hook_content_type_render().
 */
function excur_guide_block_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = theme('excur_guide_block');

  return $block;
}

/**
 * Implements hook_content_type_theme().
 */
function excur_guide_block_content_type_theme(&$theme, $plugin) {
  $theme['excur_guide_block'] = array(
    'path' => $plugin['path'],
    'template' => 'excur-guide-block',
  );
}

/**
 * Process variables for excur-image-slider.tpl.php.
 */
function template_preprocess_excur_guide_block(&$vars) {
  module_load_include('inc', 'privatemsg', 'privatemsg.pages');
  $node = menu_get_object();
  $guide = user_load($node->field_guide[LANGUAGE_NONE][0]['target_id']);

  $vars['guide_image'] = l(excur_guide_logo($guide, '240x240', array('class' => array('excur-contacts-gid-photo'))),
  'user/' . $guide->uid, array('html' => TRUE));

  if (user_is_logged_in()) {
    $vars['ask_guide'] = '<a href="#" class="question-button" id="excur-contacts-guide-button" data-toggle="modal" data-target=".message-guide">' . t('Write to the guide') . '</a>';

    $form = drupal_get_form (
      'privatemsg_new',
      array($guide),
      t('Question on the proposal "!title"', array('!title' => $node->title))
    );

    $form['body'] = $form['body']['value'];
    $form['body']['#attributes'] = array('style' =>'height: 145px');
    $form['recipient']['#attributes']['disabled'] = TRUE;
    $form['recipient']['#prefix'] = '<div class="element-hidden">';
    $form['recipient']['#suffix'] = '</div>';
    $form['actions']['submit']['#attributes']['class'][] = 'btn';
    unset($form['token']);
    unset($form['actions']['cancel']);

    $vars['form'] = theme('modal_empty', array('content' => drupal_render($form)));

  }
  else {
    $vars['ask_guide'] = '<a href="#" class="question-button excur-not-login" id="excur-contacts-guide-button">'
                            . t('Write to the guide') .
                         '</a>';

  }

}
