<?php
/**
 * @file
 * Ctools content type new_message.
 */

/**
 * Implements hook_ctools_content_types().
 */
function excur_banners_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Banners'),
    'all contexts' => TRUE,
    'category' => t('Excur'),
    'hook theme' => 'excur_banners_content_type_theme',
  );
}

/**
 * Implements hook_content_type_render().
 */
function excur_banners_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = theme('excur_banners');

  return $block;
}

/**
 * Implements hook_content_type_theme().
 */
function excur_banners_content_type_theme(&$theme, $plugin) {

  $theme['excur_banners'] = array(
    'path' => $plugin['path'],
    'template' => 'excur-banners',
  );
}

/**
 * Process variables for excur-banners.tpl.php.
 */
function template_preprocess_excur_banners(&$vars) {
  $term = menu_get_object('taxonomy_term', 2);

  if (!empty($term->field_banners_blocks['und'])) {
    foreach ($term->field_banners_blocks['und'] as $val) {
      $block = entity_load('field_collection_item', array($val['value']));
      $vars['bannres'][] = array(
          'img' => file_create_url($block[$val['value']]->field_ban_bl_img['und'][0]['uri']),
          'text' => $block[$val['value']]->field_ban_bl_text['und'][0]['safe_value'],
          'link' => $block[$val['value']]->field_ban_bl_text_link['und'][0]['safe_value']
      );
    }
  }
  $a=1;
}
