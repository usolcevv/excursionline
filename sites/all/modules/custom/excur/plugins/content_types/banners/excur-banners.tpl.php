<?php if(isset($variables['bannres'])):?>
  <div class="container prtners">
    <h3 class="pop-title">Рекоменуем воспользоваться</h3>

    <?php foreach($variables['bannres'] as $val):?>
      <a href="<?php print $val['link']; ?>">
        <div class="col-xs-3">
          <img src="<?php print $val['img']; ?>">
          <h2><?php print $val['text']?></h2>
        </div>
      </a>
    <?php endforeach;?>

  </div>
<?php endif;?>