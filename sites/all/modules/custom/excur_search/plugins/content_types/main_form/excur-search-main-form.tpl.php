<header class="slide">
  <div class="container intro-text">

    <?php print $logo; ?>
    <p>Выбирайте среди более чем 31 000 экскурсий и развлечений, предложений активного и познавательного отдыха по всему миру</p>

    <div class="form">
      <?php print render($form); ?>
    </div>

  </div>
</header>