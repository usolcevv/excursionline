
(function ($) {

    Drupal.ajax.prototype.commands.reloadPage = function() {
        window.location.reload();
    }

  Drupal.behaviors.excurSwitcher = {
    attach: $(function(context, settings) {
      // Language
      $('#language-switcher label').once().click(function() {
        window.location = $(this).data('url');
      });

            // Currency
            $('#currency-switcher label').once().click(function() {
                $.cookie('excur_currency', $(this).data('currency'), {expires: 86400, path: '/'});
                location.reload();
            });
        })
    };

    Drupal.behaviors.excurCustomUserLogin = {
        attach: $(function(context, settings) {
            $('#user-login-custom a.registration-button').once().click(function() {
                var $trueForm = $(this).closest('form');
                $('.form-actions input.form-submit', $trueForm).click();
            });
        })
    };

    Drupal.behaviors.excurCustomUserRegister = {
        attach: $(function(context, settings) {
            $('#user-register-custom a.registration-button').once().click(function() {
                var $trueForm = $(this).closest('form');
                $('.form-actions input.form-submit', $trueForm).click();
            });
        })
    };

    Drupal.behaviors.excurFlipContinents = {
        attach: $(function(context, settings) {
            $('.hover').once().hover(function() {
                $(this).addClass('flip');
            }, function() {
                $(this).removeClass('flip');
            });
        })
    };

    Drupal.behaviors.excurContinentCode = {
        attach: $(function(context, settings) {
            $('body').once('continent-id', function() {
                console.log(Drupal.settings.continentCode);
                if (Drupal.settings.continentCode != undefined) {
                    $('body').attr('id', Drupal.settings.continentCode);
                }
            });
        })
    };

    Drupal.behaviors.excurOwlCarousel = {
        attach: $(function(context, settings) {
            $('#owl-demo').once('country-cities', function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    navigationText: [
                        "",
                        ""
                    ]
                });
            });
        })
    };

    Drupal.behaviors.excurScrollTo = {
        attach: $(function(context, settings) {
            $('#excur-order-button').click(function() {
                var id = $(this).attr('href');
                $(id).ScrollTo();
            });
        })
    };

    Drupal.behaviors.excurServiceCategory = {
        attach: $(function(context, settings) {
            $('section.excur-list-category a').once().click(function(e) {
                e.preventDefault();
                var tid = $(this).data('tid');

                if ($(this).hasClass("active-category-item")) {
                    $(this).removeClass('active-category-item');
                    $('#views-exposed-form-content-city-service #edit-tid option[value='+tid+']').attr('selected', false);
                }
                else {
                    $(this).addClass('active-category-item');
                    $('#views-exposed-form-content-city-service #edit-tid option[value='+tid+']').attr('selected', 'selected');
                }


                $('#views-exposed-form-content-city-service #edit-submit-content').click();
            });
        })
    };

    Drupal.behaviors.excurWriteGuide = {
        attach: $(function() {
            $('.excur-not-login').click(function(){
                $('.head-reg').trigger('click');
            });
        })
    }

    Drupal.behaviors.excurButBook = {
        attach: $(function () {
            $('#edit-submit--3').click(function(){
                alert('ok');
                var id = $(this).data('sub-id');
                console.log(id);
                $('#service-offer-booking-' + id).trigger('click');
            });
        })
    }

    Drupal.behaviors.excurBookingOffers = {

        attach: $(function() {

            var classTickets = $('.edit-tickets'),
                class_t,
                class_t_ID;

            for(var i = 0; i < classTickets.length; i++) {
                class_t  = classTickets[i];
                class_t_ID = class_t.attributes[4].value.split('-')[2];
                var valClass = $('#edit-tickets-' + class_t_ID).val()
                $('#input-ticket-' + class_t_ID).val(valClass);
            }

            var tickets = $('.sum-price'),
                offID,
                off;
            for(var j = 0; j < tickets.length; j++) {
                off  = tickets[j];
                offID = off.id.split('-')[3];
                var oId = offID.split('_');
                var oSum = 0,
                    price;
                var count;
                for(var k in oId) {
                    count = parseInt($('#input-ticket-' + oId[k]).val());
                    price = parseFloat($('#ticket-price-' + oId[k]).html());
                    oSum += count * price;
                }
                $('#sum-price-offer-' + offID).html(oSum.toFixed(2));
            }

            $('.icon-minus').click(function() {

                var num = $(this).attr('id').split('-')[2];

                var count = Number($('#input-ticket-' + num).val());

                //var newCount = count - 1;

                var numSum = $(this).data('sum');
                var price = Number($('#ticket-price-' + num).html());
                var sum = Number($('#sum-price-offer-' + numSum).html());



                if (count >= 1 && (sum - price) > 0) {
                    sum = sum - price;

                    $('#sum-price-offer-' + numSum).html(sum.toFixed(2));

                    //$('#input-ticket-' + num).val(newCount);
                    $('#edit-tickets-' + num).val(count);


                }


            });

            $('.icon-plus').click(function() {
                var num = $(this).attr('id').split('-')[2];

                var count = Number($('#input-ticket-' + num).val());
                console.log(count);
                var newCount = count++;

                $('#input-ticket-' + num).val(newCount);
                $('#edit-tickets-' + num).val(newCount);

                var numSum = $(this).data('sum');

                var price = Number($('#ticket-price-' + num).html());
                var sum = Number($('#sum-price-offer-' + numSum).html());
                sum = sum + price;
                $('#sum-price-offer-' + numSum).html(sum.toFixed(2));

            });

            $(document).on('click', '#pay-back', function(){
                $('#pay-form-step-two').hide();
                $('#pay-form-step-one').show();
            });


        })
    }


    Drupal.behaviors.excurOfferPayEnd = {
        attach: $(function () {

            $(document).on('click', '#offer-pay-end', function() {
                var offId = $(this).data('offer-id');
                console.log(offId);
                var data = {
                    offer_id: offId
                };

                $.post(
                    '/offer_pay_end',
                    {
                        offer_id: offId
                    },
                    function (data){
                        if(data){
                            $('#pay-form-step-two').html(data);
                        }
                        else{
                            alert('false');
                        }
                    }
                );

            });

        })
    };

    Drupal.behaviors.excurChatMessage = {
        attach: $(function () {


            /*
             $(document).on('click', '#send-message-button',function () {
             var mess = $('#btn-input').val(),
             th = $(this).data('th');

             $.ajax({
             url: '/excur/msg_reply',
             type: 'POST',
             data: {
             mess: mess,
             th: th
             },

             success: function(response) {

             },

             error: function(response) {
             console.log(response);
             }

             });
             });*/

        })
    };


    Drupal.behaviors.excurUserRoom = {
        attach: $(function () {

            // Выбор аватара
            $(document).on('click', '#avatar-select', function () {

                $('#edit-field-image-und-0-upload').trigger('click');

            });

        })
    }

    Drupal.behaviors.excurProfileGuideCompany = {
        attach: $(function(context, settings) {
            $('#edit-guide').click(function() {

                if($(this).prop('checked')) {

                    $('#radio-guide-company').show();

                }else {
                    $('#radio-guide-company').hide();

                    $('#edit-company-1').removeAttr("checked");
                    $('#edit-company-2').removeAttr("checked");

                    $('#div-field-company-name').hide();
                    $('#edit-field-company-name-und-0-value').val('');
                }

            });


            $('#edit-company-2').click(function() {

                $('#div-field-company-name').show();

            })

            $('#edit-company-1').click(function() {

                $('#div-field-company-name').hide()
                $('#edit-field-company-name-und-0-value').val('');

            })

        })
    };



    Drupal.behaviors.excurGetPDF = {
        attach: $(function () {

            $(document).on('click', '.get-ticket',function () {

                var id = $(this).data('offer-id');
                console.log(id);

                $.ajax({
                    url: '/excur_offer/get_ticket?id',
                    type: 'GET',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        if (response === 'user') {
                            $('.close-modal').trigger('click');
                            $('.head-reg').trigger('click');
                        }
                        else {
                            var link = document.createElement('a');
                            link.href = response;
                            console.log(response);
                            if (link.download !== undefined) {
                                var fileName = response.substring(response.lastIndexOf('/') + 1, response.length);
                                link.download = fileName;
                            }
                            if (document.createEvent) {
                                var e = document.createEvent('MouseEvents');
                                e.initEvent('click' ,true ,true);
                                link.dispatchEvent(e);
                            }

                            var query = '?download';
                            //window.open(response + query, '_self');
                        }
                    },

                    error: function(response) {
                        console.log(response);
                    }
                });
            });
        })
    }


    Drupal.behaviors.excurGetPDF = {
        attach: $(function () {

            function guideConfirmedOffer(status, $this) {
                var id = $this.data('off-id');
                $.ajax({
                    url: '/excur_offer/confirmed_rejct',
                    type: 'POST',
                    data: {
                        id: id,
                        status: status
                    },
                    success: function(response) {
                        console.log(response);
                        $('#status-offer-'+ id +' .field-content').html(response);
                    },

                    error: function(response) {
                        console.log(response);
                    }
                });
            }

            $(document).on('click', '.guide-service-confirmed',function () {
                guideConfirmedOffer(1, $(this));
            });

            $(document).on('click', '.guide-service-rejected',function () {
                guideConfirmedOffer(0, $(this));
            });
        })
    }


    Drupal.behaviors.excurGetInfoOrder = {
        attach: $(function () {

            $(document).on('click', '.booking-list-button',function () {
                var id = $(this).data('off-id');
                console.log(id);
                $.ajax({
                    url: '/excur_offer/get_info_order',
                    type: 'POST',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('#modal-book').html(response);
                    },

                    error: function(response) {
                        console.log(response);
                    }
                });
            });

        })
    }



    Drupal.behaviors.excurGetInfoOrder = {
        attach: $(function () {

            $(document).on('click', '.booking-list-button',function () {
                var id = $(this).data('off-id');
                console.log(id);
                $.ajax({
                    url: '/excur_offer/get_info_order',
                    type: 'POST',
                    data: {
                        id: id
                    },
                    success: function(response) {
                        $('#modal-book').html(response);
                    },

                    error: function(response) {
                        console.log(response);
                    }
                });
            });

        })
    }


})(jQuery);
