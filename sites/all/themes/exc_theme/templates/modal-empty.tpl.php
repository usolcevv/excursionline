<div class="content-modal-container empty-modal-container mt-10">
  <div class="col-xs-12 empty-wright-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      <?php print  theme('image', array(
          'path' => EXCUR_FRONT_THEME_PATH . '/images/close.png',
          'attributes' => array(
            'class' => array('close-modal-img'),
            ),
          )
      ); ?>
    </button>
    <?php print $content; ?>
    </div>
</div>