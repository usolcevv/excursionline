<div class="col-xs-3">
  <aside class="user-profile-sidebar">
<!--    <div class="user-profile-avatar text-center" id="user-avatar">
      <?php /*print $photo; */?>
    </div>
    <div class="text-change-avatar" id="avatar-select">
      Изменить фото
    </div>
    <div class="change-avatar">
    </div>-->
    <div class="user-profile-avatar text-center" id="user-avatar">
      <?php print $photo; ?>
    </div>
    <a href="#" data-toggle="modal" data-target=".avatar">
      <div class="text-change-avatar">
          Изменить фото
      </div>
      <div class="change-avatar">
      </div>
    </a>
    <ul class="list user-profile-nav">
      <?php foreach($menu as $item): ?>
        <li>
          <?php print $item ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </aside>
</div>

<div class="element-hidden">
  <form name="uploadimages" method="post" enctype="multipart/form-data">
    <input id="uploadimage" type="FILE" name="uploadimage" id="uploadfile">
  </form>
</div>