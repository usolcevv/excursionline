<div class="col-xs-12">
  <label>Тема </label>
</div>
<div class="col-xs-12">
  <?php print render($form['subject']); ?>
</div>
<div class="col-xs-12">
  <label for="edit-body-value">Сообщение </label>
</div>
<div class="col-xs-12">
  <?php print render($form['body']); ?>
</div>
<div class="col-xs-12" style="margin-top: 5px">
  <?php print render($form['actions']['submit']);?>
</div>


<div class="hidden">
<?php print drupal_render_children($form);?>
</div>