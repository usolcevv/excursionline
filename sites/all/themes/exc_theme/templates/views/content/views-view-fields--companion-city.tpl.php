<div class="col-xs-3 city-travel-item">
  <div class="city-travel-item-inner">
    <?php print $image; ?>
    <div class="pop-excur-row">
      <div class="col-xs-3 pop-excur-star">
        <img src="<?php print file_create_url(EXCUR_FRONT_THEME_PATH . '/images/star.png');?>" class="iconstar">&nbsp;&nbsp;&nbsp;<?php print $rating; ?>
      </div>
      <div class="col-xs-9 pop-excur-language">
        <?php foreach($languages as $lang): ?>
          <img class="flag flag-<?php print $lang; ?>"/>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="pop-excur-name">
      <?php print $title ?>
    </div>
    <div class="pop-excur-price">
      <?php print $price; ?>
      <span class="currency">
        <?php print $currency; ?>
      </span>
    </div>
    <a href="<?php print $link;?>">
      <div class="travel-city-about" >
        <img src="<?php print file_create_url(EXCUR_FRONT_THEME_PATH . '/images/question.png');?>" class="iconq">&nbsp&nbsp<?php print t('Подробней'); ?>
      </div>
    </a>
  </div>
</div>
