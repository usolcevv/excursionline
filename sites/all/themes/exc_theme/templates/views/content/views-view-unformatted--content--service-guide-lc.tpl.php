
<div class="col-xs-12 excur-list-line offer-line">
</div>
<?php foreach($view->result as $key => $val): ?>
<div class="col-xs-12 excur-list-content offer-list">
  <a href="#">
    <div class="col-xs-3 excur-list-image">
      <img src="<?php print file_create_url($view->result[0]->field_field_image[0]['raw']['uri']); ?>" class="excur-image-booking">
      <div class="col-xs-12 excur-list-button offer-list-button">
        <a href="/node/<?php print $val->nid; ?>" class="button-go booking-list-button">Подробности</a>
        <a href="/node/<?php print $val->nid; ?>/edit" class="button-go offer-list-but">Правка</a>
        <a href="/node/<?php print $val->nid; ?>/delete" class="button-go delet-offer-button">Удалить</a>

      </div>
    </div>
  </a>
  <div class="col-xs-9 excur-list-infoblock-offer">
    <div class="col-xs-10 excur-list-title">
      <a href="#">
        <div class="excur-list-name booking-list-name">
          <?php print $val->_field_data['nid']['entity']->title; ?>
        </div>
      </a>
    </div>
    <div class="col-xs-5 offer-list-info">
      <div class="col-xs-12  offer-list-date">
        <img  src="/<?php print EXCUR_FRONT_THEME_PATH?>/images/time.png" class="iconcalnd offer-time-icon">&nbsp
        <?php print $val->field_data_field_duration_field_duration_value; ?>
      </div>
    </div>

    <div class="col-xs-3 offer-list-info">
      <div class="col-xs-4 excur-list-lang offer-list-lang">
        <div class="pop-excur-gid-name">
          <img class="flag flag-<?php print $val->field_field_languages[0]['raw']['taxonomy_term']->field_lang_code['und'][0]['value']?>"/>
        </div>
      </div>
    </div>

    <div class="col-xs-4 excur-list-info offer-list-info">
      <div class="excur-price offer-price">
        <?php print $val->field_collection_item_field_data_field_offer_ticket__field_d;?>
        <span class="currency"> <?php print 'eur';?></span>
      </div>
    </div>
    </div>
  </div>
<?php endforeach; ?>