<div class="col-xs-3 travel-companion-item">
  <p class="travel-companion-name">
    <?php print render($name) ?>
  </p>
  <?php print render($image) ?>
  <?php print render($message) ?>
</div>