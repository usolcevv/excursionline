<?php

/**
 * @file
 */
?>

<section id="about-ex" data-speed="2" data-type="background" class="hidden-sm hidden-xs">
  <h2 class="about-title">
    <?php print t('Most popular excursions'); ?>
  </h2>
  <div class="container">
    <div id="owl-demo" class="owl-carousel owl-theme">
      <?php foreach ($rows as $id => $row): ?>
      <div class="item">
        <?php print $row; ?>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>