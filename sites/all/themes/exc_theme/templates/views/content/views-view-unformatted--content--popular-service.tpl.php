<?php

/**
 * @file
 */
?>

<section class="pop-excur">
  <div class="pop-excur-content">
    <h1 class="pop-title title-country">
      <?php print t('Most popular excursions'); ?>
    </h1>
    <h2 class="pop-title-prefix">
      <?php print $country; ?>
    </h2>
    <div id="owl-demo" class="owl-carousel owl-theme">
      <?php foreach ($rows as $id => $row): ?>
        <div class="item">
          <?php print $row; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<?php if(!empty($term->field_img_baner_1['und'])):?>
  <a href="<?php print $term->field_link_banner_1['und'][0]['safe_value'];?>">
    <div class="container avia-parnters prtn">
      <img src="<?php print file_create_url($term->field_img_baner_1['und'][0]['uri']); ?>">
      <h2><?php print $term->field_text_banner_1['und'][0]['safe_value']; ?></h2>
      <a class="prnt-al" href="<?php print $term->field_link_banner_1['und'][0]['safe_value'];?>">Перейти<br> к поиску</a>
    </div>
  </a>
<?php endif;?>