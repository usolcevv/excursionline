
<div class="col-xs-12 excur-list-content booking-list">
  <a href="#">
    <div class="col-xs-3 excur-list-image">
      <?php print $image; ?>
      <div class="col-xs-12 excur-list-button offer-list-button">
        <?php print $details; ?>
      </div>
    </div>
  </a>
  <div class="col-xs-9 excur-list-infoblock-offer">
    <div class="col-xs-12 excur-list-title">
      <a href="<?php print $path; ?>">
        <div class="excur-list-name booking-list-name">
          <?php print $title; ?>
        </div>
      </a>
      </a>
    </div>
    <div class="col-xs-4 booking-list-info">
      <div class="col-xs-12 booking-list-star">
        id <?php print $id; ?>
      </div>
      <div class="col-xs-12  booking-list-date">
        <?php print $img_calendar; ?>&nbsp<?php print $data; ?>
      </div>
    </div>
    <div class="col-xs-4 booking-list-gid">

    </div>
    <div class="col-xs-4 booking-list-info-status">
      <?php print $status; ?>
    </div>
  </div>
</div>

<div class="col-xs-12 excur-list-line offer-line">
</div>
